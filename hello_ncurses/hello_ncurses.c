#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
int main()
{
    WINDOW *mainwin;

    /* start and check ncurses mode */
    if ((mainwin = initscr()) == NULL)
    {
        fprintf(stderr, "ncurses initialization failed.");
        return EXIT_FAILURE;
    }
    printw("Hello ncurses !"); /* print string in a buffer */
    refresh();                 /* print on to the real screen */
    getch();                   /* wait for a key input */

    endwin(); /* end curses mode */

    return EXIT_SUCCESS;
}
