/**
 * @file ex1.c
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief 
 * @version 0.1
 * @date 2020-02-06
 * 
 * @copyright CFPT (c) 2020
 * 
 */
#include <stdio.h>
#include <stdlib.h>

#define FILE_NAME "poids.txt"

/**
 * @brief Compute the average weight of a population.
 * 
 * The file must be formated like this:
 * ```
 * 1
 * 40
 * 53
 * 57
 * ```
 * Where the first line = the number of people that weigh 1 kilo,
 * second line = the number of people that weigh 2 kilos,
 * ect.
 * 
 * @param filename file to read for the population by weight
 * @return long 
 */
long compute_avg_weight(char filename[])
{
    FILE *fp;
    long population = 0, totalWeight = 0;
    int input, i = 1;
    int tmp = 1;

    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        exit(EXIT_FAILURE);
    }
    printf("File opened\n");

    while (fscanf(fp, "%d", &input) == 1)
    {
        population += input;
        //i = the line we are on
        totalWeight += input * i;

        i++;
    }

    printf("Computed \n");
    printf("population : %ld\n", population);
    printf("totalWeight : %ld\n", totalWeight);

    return totalWeight / population;
}

int main()
{
    long avg = compute_avg_weight(FILE_NAME);
    printf("Moyenne de poid: %ld\n", avg);
    return EXIT_SUCCESS;
}