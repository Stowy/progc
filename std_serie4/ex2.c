#include <stdio.h>
#include <stdlib.h>

#define MAX_ARRAY_SIZE 200
#define FILENAME "produit.txt"

/**
 * @brief Empties the given array.
 * 
 * @param array 
 * @param size 
 */
void empty_array(float array[], size_t size)
{
    size_t i;
    for (i = 0; i < size; i++)
    {
        array[i] = 0;
    }
}

/**
 * @brief loads the products into the array
 * 
 * The file must be formated like so:
 * ```
 * 3 50.000000
 * 7 1546.000000
 * ```
 * the first number is the product id and the second is the price
 * 
 * @param filename 
 * @param array The array that will store the products.
 * @param arraySize 
 */
void load_products(char filename[], float array[], size_t arraySize)
{
    FILE *fp;
    int index;
    float value;

    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        exit(EXIT_FAILURE);
    }

    empty_array(array, arraySize);

    while (fscanf(fp, "%d %f", &index, &value) != -1)
    {
        array[index] = value;
    }

    fclose(fp);
}

/**
 * @brief Get the price from the code of the product.
 * 
 * @param array 
 * @param code 
 * @return float 
 */
float get_price(float array[], int code)
{
    return array[code];
}

/**
 * @brief Adds a product to the array
 * 
 * @param array 
 * @param code 
 * @param price 
 */
void add_product(float array[], int code, float price)
{
    array[code] = price;
}

void save_array(float array[], size_t size, char filename[])
{
    FILE *fp;
    size_t i;

    fp = fopen(filename, "w+");
    if (fp == NULL)
    {
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < size; i++)
    {
        if (array[i] > 0)
        {
            fprintf(fp, "%d %f", &i, array[i]);
        }
    }

    printf("Saved.");
    fclose(fp);
}

int main()
{
    float array[MAX_ARRAY_SIZE];
    load_products(FILENAME, array, MAX_ARRAY_SIZE);
    unsigned int choice;

    do{
        printf("1: Ajouter un produit.\n2: Consulter un prix.\n3: Quitter\n");
    }

    return EXIT_SUCCESS;
}