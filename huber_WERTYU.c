/**
 * @file huber_WERTYU.c
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief WERTYU challenge of onlinejudge.org
 *
 * Usage : ./exefile < testfile.txt
 * testfile.txt contains a string which charactesr are shifted
 * of one row in a qwerty keyboard
 *
 * @version 0.1
 * @date 2019-12-12
 *
 * @copyright CFPT (c) 2019
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SENTENCE_LENGTH_MAX 1000
#define KEYBOARD_CHARS_ARRAY_LENGTH 48

/**
 * @brief find the letter in the array
 *
 * @param letter letter to get the index for
 * @param keyboardChars the array to search for the letter
 * @return returns the index if it is in the array, otherwise return -1
 */
static int getLetterIndex(char letter, char keyboardChars[]) {
    int i;
    for (i = 0; i < KEYBOARD_CHARS_ARRAY_LENGTH; i++) {
        if (letter == keyboardChars[i]) {
            return i;
        }
    }

    return -1;
}

int main() {
    /* getline() execute a malloc if destination buffer pointer is NULL */
    char *sentence = NULL;
    char keyboarChars[KEYBOARD_CHARS_ARRAY_LENGTH] =
        "`1234567890-=QWERTYUIOP[]\\ASDFGHJKL;'ZXCVBNM,./";
    unsigned int i;
    int letterIndex;
    size_t nbbytes;

    /* unrecog is a splint directive in order to avoid warning on getline
     * function
     */
    while (/*@-unrecog@*/ getline(&sentence, &nbbytes, stdin) != EOF) {
        /* sentence contains all characters of one line */
        for (i = 0; i < /*@-null@*/ (unsigned int)strlen(sentence); ++i) {
            /* if the letter is in the array, we store it and print it with the
             * correct letters */
            letterIndex = getLetterIndex(sentence[i], keyboarChars);
            if (letterIndex != -1) {
                printf("%c", keyboarChars[letterIndex - 1]);
            } else {
                /* If it's not in the array, we just print the char (ex: for the
                 * spaces) */
                printf("%c", sentence[i]);
            }
        }

    } /* end while */

    /* dealllocate memory allocated automatically by getline() */
    free(sentence);
    return 0;
}
