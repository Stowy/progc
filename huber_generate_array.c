/**
 * @file huber_generate_array.c
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Will generate an array to use to solve the WERTYU challenge on onlinejudge.org
 *
 * Usage : ./exefile 
 *
 * @version 0.1
 * @date 2019-12-16
 *
 * @copyright CFPT (c) 2019
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INPUT_KEYBOARD_ARRAY_MAX 256
#define DEFAULT_CHAR (char)32

int main() {
    //Char that is inputed
    char origChar[] = "1234567890-=WERTYUIOP[]\\SDFGHJKL;'XCVBNM,./";
    //Char that is outputed
    char outputChar[] = "`1234567890-QWERTYUIOP[]ASDFGHJKL;ZXCVBNM,.";
    int i;
    size_t j;

    printf("{");
    for (i = 0; i < INPUT_KEYBOARD_ARRAY_MAX; i++) {
        printf("'");
        // char to print by default
        char mychar = DEFAULT_CHAR;
        // Loop through all the chars in the origChar array
        for (j = 0; j < strlen(origChar); j++) {
            // if the actual index is equal to an originalChar, we put the
            // outputChar at the same index in mychar and print it
            if (i == (int)origChar[j]) {
                mychar = outputChar[j];
            }
        }
        printf("%c", mychar);
        printf("', ");
    }
    printf("}\n");

    return 0;
}