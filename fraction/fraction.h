/**
 * @file fraction.h
 * @author Fabian HUBER (fabian.hbr@eduge.ch)
 * @brief
 * @version 0.1
 * @date 2020-01-13
 *
 * @copyright CFPT (c) 2020
 *
 */

#ifndef FRACTION_H
#define FRACTION_H

typedef struct Fraction_s
{
    int numerator;
    int denominator;
    int (*GetNumerator)(struct Fraction_s *fraction);
    int (*GetDenominator)(struct Fraction_s *fraction);
    void (*SetFraction)(struct Fraction_s *fraction, int numerator, int denominator);
    struct Fraction_s(*Add)(struct Fraction_s *fraction, struct Fraction_s *fraction2);
    void (*Print)(struct Fraction_s *fraction);
} Fraction;

void InitFraction(Fraction *fraction, int numerator, int denominator);

#endif /* MAIN_H */
       /* EOF */
