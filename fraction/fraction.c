/**
 * @file fraction.c
 * @author Fabian HUBER (fabian.hbr@eduge.ch)
 * @brief
 * Methods to set values in a Fraction struct
 * @version 0.1
 * @date 2020-01-13
 *
 * @copyright CFPT (c) 2020
 *
 */

#include "fraction.h"
#include <stdio.h>
#include <stdlib.h>

void SetFraction(Fraction *fraction, int numerator, int denominator)
{
    if (denominator == 0)
    {
        printf("denominator cannot be null.");
        return;
    }

    fraction->numerator = numerator;
    fraction->denominator = denominator;
}

int GetNumerator(Fraction *fraction) { return fraction->numerator; }

int GetDenominator(Fraction *fraction) { return fraction->denominator; }

void Print(Fraction *fraction)
{
    printf("%d / %d", GetNumerator(fraction), GetDenominator(fraction));
}

static Fraction *Add(Fraction *fraction, Fraction *fraction2){
    Fraction *addedFraction = (Fraction*)malloc(sizeof(Fraction));
    int addedDenominator = GetDenominator(fraction) * GetDenominator(fraction2);
    int tempNumerator = GetDenominator(fraction2) * GetNumerator(fraction);
    int addedNumerator = GetDenominator(fraction) * GetNumerator(fraction2);
    addedNumerator += tempNumerator;
    InitFraction(addedFraction, addedNumerator, addedDenominator);

    return addedFraction;
}

void InitFraction(Fraction *fraction, int numerator, int denominator)
{
    fraction->SetFraction = SetFraction;

    fraction->GetNumerator = GetNumerator;
    fraction->GetDenominator = GetDenominator;

    fraction->Print = Print;

    fraction->SetFraction(fraction, numerator, denominator);
}
