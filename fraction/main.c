/**
 * @file main.c
 * @author Fabian HUBER (fabian.hbr@eduge.ch)
 * @brief
 *
 * @version 0.1
 * @date 2020-01-13
 *
 * @copyright CFPT (c) 2020
 *
 */
#include "fraction.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{
    Fraction *aFraction, *bFraction, *resultFraction;
    aFraction = (Fraction*)malloc(sizeof(Fraction));
    bFraction = (Fraction*)malloc(sizeof(Fraction));
    
    /* call "object" constructors */
    InitFraction(aFraction, 1, 2);
    InitFraction(bFraction, 0, 1);

    printf("aFraction : num = %d\n", aFraction->GetNumerator(aFraction));
    printf("aFraction : den = %d\n", aFraction->GetDenominator(aFraction));

    bFraction->SetFraction(bFraction, 1, 4);
    printf("bFraction = ");
    bFraction->Print(bFraction);
    printf("\n");

    resultFraction = (Fraction*)aFraction->Add(aFraction, bFraction);
    printf("aFraction + bFraction = ");
    resultFraction->Print(resultFraction);
    printf("\n");

    // each dynamic memory area MUST be freed
    free(aFraction);
    free(bFraction);
    free(resultFraction);
    return 0;
}
