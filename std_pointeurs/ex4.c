#include <stdio.h>

void computeSumAndProduct(int array[], int arraySize, int *sum, int *product){
    for (size_t i = 0; i < arraySize; i++)
    {
        *sum += array[i];
        *product *= array[i];
    }
    
}

int main() {
    size_t arraySize;
    int sum = 0, product = 1;
    printf("Entrez le nombre d'éléments : ");
    scanf("%zu", &arraySize);

    int array[arraySize];

    printf("Veillez saisir les elements du tableau\n");
    for (size_t i = 0; i < arraySize; i++) {
        scanf("%d", &array[i]);
    }

    computeSumAndProduct(array, arraySize, &sum, &product);

    printf("somme = %d et produit = %d\n", sum, product);

    return 0;
}