#include <stdio.h>


void exangeNumbers(float *a, float *b){
    float temp = *a;
    *a = *b;
    *b = temp;
}

int main(){
    float a, b;
    printf("Valeur de a : ");
    scanf("%f", &a);
    printf("Valeur de b : ");
    scanf("%f", &b);

    printf("a = %f; b = %f\n", a, b);

    printf("Echange\n");
    exangeNumbers(&a, &b);
    printf("a = %f; b = %f\n", a, b);

    return 0;
}