/**
 * @file func.c
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief File where all the functions of the program are.
 * @version 0.1
 * @date 2020-02-27
 * 
 * @copyright CFPT (c) 2020
 * 
 */
#include "func.h"
#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief Saves the array of students in a file
 * 
 * @param students The array of students to save.
 * @param nbrStudents The number of students in the array.
 */
static void save_students(Student students[MAX_NBR_STUDENTS], int nbrStudents)
{
    FILE *fp;
    size_t i;

    //Load the file in write mode
    fp = fopen(FILENAME, "w+");
    if (fp == NULL)
    {
        exit(EXIT_FAILURE);
    }

    //Save the info
    for (i = 0; i < nbrStudents; i++)
    {
        fprintf(fp, "%d %s %s\n", students[i].num, students[i].lastname, students[i].firstname);
    }

    //Close the file
    fclose(fp);
}

/**
 * @brief Reads the data from the file and displays it.
 * 
 */
void read()
{
    FILE *fp;
    Student students[MAX_NBR_STUDENTS];
    int inputNum;
    char inputFirstname[MAX_NAME_SIZE] = {'\0'};
    char inputLastname[MAX_NAME_SIZE] = {'\0'};
    size_t i = 0;
    size_t j;

    //Load the file in read mode.
    fp = fopen(FILENAME, "r");
    if (fp == NULL)
    {
        exit(EXIT_FAILURE);
    }

    printf("*** Contenu du fichier %s ***\n", FILENAME);

    //Load the data in the array.
    while (fscanf(fp, "%d %s %s", &inputNum, inputLastname, inputFirstname) != -1)
    {
        students[i].num = inputNum;
        strcpy(students[i].lastname, inputLastname);
        strcpy(students[i].firstname, inputFirstname);
        i++;
    }

    //Print the data of the array;
    for (j = 0; j < i; j++)
    {
        printf("Matricule : %d        Nom et prénom : %s %s\n", students[j].num, students[j].lastname, students[j].firstname);
    }
    
}

/**
 * @brief Asks the user to create students and then saves them.
 * 
 */
void create()
{
    Student students[MAX_NBR_STUDENTS];
    int inputNbrStudents;
    int inputNum;
    char inputFirstname[MAX_NAME_SIZE] = {'\0'};
    char inputLastname[MAX_NAME_SIZE] = {'\0'};
    size_t i;

    printf("*** Creation du fichier %s ***\n", FILENAME);

    //Read the number of students to save in the arrays
    printf("Nombre d'enregistrements à créer : ");
    scanf("%d", &inputNbrStudents);

    for (i = 0; i < inputNbrStudents; i++)
    {
        printf("Enregistrement No : %zu\n", i + 1);

        //Read num
        printf("Numéro de matricule : ");
        scanf("%d", &inputNum);
        students[i].num = inputNum;

        //Read Lastname
        printf("Nom : ");
        scanf("%s", inputLastname);

        //Copy the string
        strcpy(students[i].lastname, inputLastname);

        //Read firstname
        printf("Prénom : ");
        scanf("%s", inputFirstname);

        //Copy the string
        strcpy(students[i].firstname, inputFirstname);
    }

    printf("\n");

    //Save the array in a file.
    save_students(students, inputNbrStudents);
}
