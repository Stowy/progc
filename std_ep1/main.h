/**
 * @file main.h
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Header of main.c
 * @version 0.1
 * @date 2020-02-27
 * 
 * @copyright CFPT (c) 2020
 * 
 */
#ifndef MAIN_H
#define MAIN_H

#define MAX_NAME_SIZE 50

typedef struct Student_s
{
    int num;
    char lastname[MAX_NAME_SIZE];
    char firstname[MAX_NAME_SIZE];
} Student;

#endif /* MAIN_H */
       /* EOF */