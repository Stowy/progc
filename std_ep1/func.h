/**
 * @file func.h
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Header of func.c
 * @version 0.1
 * @date 2020-02-27
 * 
 * @copyright CFPT (c) 2020
 * 
 */
#ifndef FUNC_H
#define FUNC_H

#define MAX_NBR_STUDENTS 14
#define FILENAME "info.txt"

void create();
void read();

#endif /* FUNC_H */
       /* EOF */