/**
 * @file main.c
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Main file of the program.
 * @version 0.1
 * @date 2020-02-27
 * 
 * @copyright CFPT (c) 2020
 * 
 */
#include "main.h"
#include "func.h"

int main()
{
    create();
    read();
}