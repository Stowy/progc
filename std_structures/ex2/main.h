/**
 * @file main.h
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Header of main.c
 * @version 0.1
 * @date 2020-02-20
 * 
 * @copyright CFPT (c) 2020
 * 
 */
#ifndef MAIN_H
#define MAIN_H

#define MAX_PRODUCTS 50

#define CHOICE_CREATE 1
#define CHOICE_DISPLAY 2
#define CHOICE_QUIT 3

typedef struct Product_s
{
    int code;
    int reference;
    double price;
    int quantity;
} Product;

#endif /* MAIN_H */
       /* EOF */
