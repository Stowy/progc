#include "func.h"
#include "main.h"
#include <stdio.h>
#include <stdlib.h>

void initProductArray(Product products[], size_t size)
{
    size_t i;
    for (i = 0; i < size; i++)
    {
        products[i].code = DEFAULT_CODE;
        products[i].price = DEFAULT_PRICE;
        products[i].quantity = DEFAULT_QUANTITY;
        products[i].reference = DEFAULT_REFERENCE;
    }
}

Product *createProduct()
{
    Product *product = (Product *)malloc(sizeof(Product));
    int inputCode;
    int inputReference;
    double inputPrice;
    int inputQuantity;

    //Read code
    printf("Entrez le code du produit : ");
    scanf("%d", &inputCode);
    product->code = inputCode;

    //Read reference
    printf("Entrez la reference : ");
    scanf("%d", &inputReference);
    product->reference = inputReference;

    //Read price
    printf("Entrez le prix : ");
    scanf("%lf", &inputPrice);
    product->price = inputPrice;

    //Read quantity
    printf("Entrez la quantite en stock : ");
    scanf("%d", &inputQuantity);
    product->quantity = inputQuantity;

    return product;
}

void displayProduct(Product *product)
{
    printf("Type : %d\n", product->code);
    printf("Reference : %d\n", product->reference);
    printf("Prix : %lf\n", product->price);
    printf("Quantite en stock : %d\n\n", product->quantity);
}

void displayProductFromReference(Product products[], size_t size, int reference)
{
    size_t i;
    for (i = 0; i < size; i++)
    {
        if (products[i].reference == reference)
        {
            displayProduct(&products[i]);
        }
    }
}

void displayProducts(Product products[], size_t arraySize)
{
    size_t i;
    for (i = 0; i < arraySize; i++)
    {
        displayProduct(&products[i]);
    }
}