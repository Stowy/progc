/**
 * @file func.h
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Header of func.c
 * @version 0.1
 * @date 2020-02-20
 * 
 * @copyright CFPT (c) 2020
 * 
 */
#ifndef FUNC_H
#define FUNC_H
#include "main.h"
#include <stdio.h>

#define DEFAULT_CODE 0
#define DEFAULT_PRICE 1
#define DEFAULT_QUANTITY 1
#define DEFAULT_REFERENCE 0

Product *createProduct();
void displayProduct(Product *product);
void displayProductFromReference(Product products[], size_t size, int reference);
void initProductArray(Product products[], size_t size);

#endif /* FUNC_H */
/* EOF */
