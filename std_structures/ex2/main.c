#include "main.h"
#include "func.h"
#include <stdio.h>

int main()
{
    Product products[MAX_PRODUCTS];
    int freePosition = 0;
    int inputChoiceMenu = 0;
    int inputSelectedProduct = 0;

    initProductArray(products, MAX_PRODUCTS);

    while (inputChoiceMenu != CHOICE_QUIT)
    {
        printf("1. Créer un nouveau produit\n");
        printf("2. Voir les données d'un produit\n");
        printf("3. Quitter\n");
        scanf("%d", &inputChoiceMenu);

        if (inputChoiceMenu == CHOICE_CREATE)
        {
            products[freePosition] = *createProduct();
            freePosition++;
        }
        else if (inputChoiceMenu == CHOICE_DISPLAY)
        {
            printf("Entrez une référence de produit à voir : ");
            scanf("%d", &inputSelectedProduct);
            displayProductFromReference(products, MAX_PRODUCTS, inputSelectedProduct);
        }

        //Empty buffer
        scanf("%*[^\n]");
        scanf("%*c");
    }
}