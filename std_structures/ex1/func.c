/**
 * @file func.c
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief A file for all the functions
 * @version 0.1
 * @date 2020-02-20
 * 
 * @copyright CFPT (c) 2020
 * 
 */
#include <stdio.h>
#include <string.h>
#include "func.h"
#include "main.h"

#define DEFAUT_LASTNAME "DEFAULT LASTNAME"
#define DEFAULT_FIRSTNAME "DEFAULT FIRSTNAME"
#define DEFAULT_AGE 1
#define DEFAULT_GENDER 0

#define GENDER_MALE 0
#define GENDER_FEMALE 1

/**
 * @brief Initilize and array of users with default values.
 * 
 * @param users The array of users.
 * @param arraySize The size of the array of users.
 */
void initUserArray(User users[], size_t arraySize)
{
    size_t i;
    for (i = 0; i < arraySize; i++)
    {
        strcpy(users[i].lastname, DEFAUT_LASTNAME);
        strcpy(users[i].firstname, DEFAULT_FIRSTNAME);
        users[i].age = DEFAULT_AGE;
        users[i].gender = DEFAULT_GENDER;
    }
}

/**
 * @brief Displays all the users in the array.
 * 
 * @param users The array of users.
 * @param arraySize The size of the array of users.
 */
void displayUsers(User users[], size_t arraySize)
{
    size_t i;

    printf("Liste des joueurs:\n\n");

    for (i = 0; i < arraySize; i++)
    {
        printf("Joueur %zu : %s %s\n", i, users[i].firstname, users[i].lastname);
        printf("Age : %d\n", users[i].age);
        if (users[i].gender == GENDER_MALE)
        {
            printf("C'est un homme.\n\n");
        }
        else if (users[i].gender == GENDER_FEMALE)
        {
            printf("C'est une femme.\n\n");
        }
    }
}