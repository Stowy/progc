/**
 * @file main.c
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief A program that will store a list of players and display it.
 * @version 0.1
 * @date 2020-02-20
 * 
 * @copyright CFPT (c) 2020
 * 
 */
#include "main.h"
#include "func.h"
#include <stdio.h>
#include <string.h>

int main()
{
    User users[MAX_PLAYERS];
    size_t i;
    char inputLastname[MAX_STRING_SIZE] = {'\0'};
    char inputFirstname[MAX_STRING_SIZE] = {'\0'};
    int inputAge;
    int inputGender;

    initUserArray(users, MAX_PLAYERS);

    for (i = 0; i < MAX_PLAYERS; i++)
    {
        //Read lastname
        printf("Quel est le nom de famille du joueur %zu? ", i);
        if (fgets(inputLastname, MAX_STRING_SIZE, stdin) != NULL)
        {
            //Remove trailing newline
            strtok(inputLastname, "\n");

            //Copy the string
            strcpy(users[i].lastname, inputLastname);
        }

        //Read firstname
        printf("Quel est le prénom du jouer %zu? ", i);
        if (fgets(inputFirstname, MAX_STRING_SIZE, stdin) != NULL)
        {
            //Remove trailing newline
            strtok(inputFirstname, "\n");

            //Copy the string
            strcpy(users[i].firstname, inputFirstname);
        }

        //Read age
        printf("Quel est l'age du jouer %zu? ", i);
        scanf("%d", &inputAge);
        users[i].age = inputAge;

        //Read gender
        printf("Quel est le sexe du jouer %zu? [0:homme, 1:femme] ", i);
        scanf("%d", &inputGender);
        users[i].gender = inputGender;

        //Empty buffer
        scanf("%*[^\n]"); 
        scanf("%*c"); 
    }

    displayUsers(users, MAX_PLAYERS);

    return 0;
}