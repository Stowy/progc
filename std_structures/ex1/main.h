/**
 * @file main.h
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Header of main.c
 * @version 0.1
 * @date 2020-02-20
 * 
 * @copyright CFPT (c) 2020
 * 
 */
#ifndef MAIN_H
#define MAIN_H

#define MAX_NAME_SIZE 50
#define MAX_PLAYERS 2
#define MAX_STRING_SIZE 50

typedef struct User_s
{
    char lastname[MAX_NAME_SIZE];
    char firstname[MAX_NAME_SIZE];
    int age;
    int gender;
} User;

#endif /* MAIN_H */
       /* EOF */
