/**
 * @file func.h
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Header of func.c
 * @version 0.1
 * @date 2020-02-20
 * 
 * @copyright CFPT (c) 2020
 * 
 */
#ifndef FUNC_H
#define FUNC_H

#include <stdio.h>
#include "main.h"

void initUserArray(User users[], size_t arraySize);
void displayUsers(User users[], size_t arraySize);

#endif /* FUNC_H */
       /* EOF */
