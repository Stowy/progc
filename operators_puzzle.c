/**
 * \file operators_puzzle.c
 * \brief Program that finds the good operators to get a desired result while using 3 times the same number.
 * 
 * The program is made to solve a puzzle where you must find the good operators to find a desired result 
 * when you use three times the same number in that operation.
 * It will brute force through all the operators and print out those that will get the good result.
 * 
 * Here's what the puzzle looks like:
 * |   | op1 |   | op2 |   | Result |
 * | - | --- | - | --- | - | :----: |
 * | 2 |     | 2 |     | 2 |    6   |
 * | 3 |     | 3 |     | 3 |    6   |
 * | 4 |     | 4 |     | 4 |    6   |
 * | 5 |     | 5 |     | 5 |    6   |
 * | 6 |     | 6 |     | 6 |    6   |
 * 
 * Output exemple:
 * ```
 * 2 + 2 + 2
 * 2 + 2 * 2
 * 2 * 2 + 2
 * 3 * 3 - 3
 * 5 + 5 / 5
 * 5 / 5 + 5
 * 6 + 6 - 6
 * 6 - 6 + 6
 * 6 * 6 / 6
 * 6 / 6 * 6
 * ```
 * 
 * \author Fabian HUBER (fabian.hbr@eduge.ch)
 * \version 1.0
 * \date 2019-11-11
 * 
 * \copyright CFPT (c) 2019
 * 
 */
#include <stdio.h>
#include <stdlib.h>

/* Constants */
#define NBR_OPERATORS 4
#define MIN 2
#define MAX 6
#define DESIRED_RESULT 6

/* We use functions for the operator so that we can swap them easily */
static int add(int nbr1, int nbr2) { return nbr1 + nbr2; }
static int substract(int nbr1, int nbr2) { return nbr1 - nbr2; }
static int multiply(int nbr1, int nbr2) { return nbr1 * nbr2; }
static int divide (int nbr1,int nbr2) { return nbr1 / nbr2; }

int main(){
    /* vars where the adresses of the current opperators will be stored */
    int (*operator1)(int, int);
    int (*operator2)(int, int);

    /* tables of all the informations nedded for the operators: adress of the functions, symbols to display and priorities */
    int (*operators[NBR_OPERATORS])(int, int) = {&add, &substract, &multiply, &divide};
    char symbols[NBR_OPERATORS] = {'+', '-', '*', '/'};
    int priorities[NBR_OPERATORS] = {1, 1, 2, 2};
    
    int i, result;
    size_t j, k;

    for (i = MIN; i <= MAX; i++)
    {
        for (j = 0; j < NBR_OPERATORS; j++)
        {
            for ( k = 0; k < NBR_OPERATORS; k++)
            {
                /* Stores the good operator */
                operator1 = operators[j];
                operator2 = operators[k];

                /* Does the good operation depending on the priorities */
                if(priorities[j] >= priorities[k]){
                    result = operator2(operator1(i, i), i);
                }
                else{
                    result = operator1(i, operator2(i, i));
                }

                /* prints the operation if it's the desired result */
                if(result == DESIRED_RESULT){
                    printf("%d %c %d %c %d\n", i, symbols[j], i, symbols[k], i);
                }
            }
        }
    }

    return 0;
}