#include <time.h>
#include <stdio.h>
#define NBR_DAY 33
#define NBR_DAYS_IN_WEEK 7
#define START_DAY 3

int main(){
    char *daysOfWeek[NBR_DAYS_IN_WEEK] = {"Monday", "Tuesday", "Wesnday", "Thursday", "Friday", "Saturday", "Sunday"};
    int dayOfWeek = START_DAY;
    int i;

    for (i = 0; i < NBR_DAY; i++)
    {
        dayOfWeek++;
        if (dayOfWeek >= NBR_DAYS_IN_WEEK){
            dayOfWeek = 0;
        }
    }

    printf("May 4th is a %s\n", daysOfWeek[dayOfWeek]);

    return 0;
}
