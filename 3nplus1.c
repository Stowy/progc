/**
 * \file 3nplus1.c
 * \brief A program that solves the 3n+1 problem for given numbers.
 * 
 * Usage: ./3nplus1 < data.txt
 * data.txt contains 2 integers per line.
 * 
 * \author Fabian HUBER
 * \date 14.10.2019, V1
 */

#include <stdio.h>

/**
 * Computes the number of cycles needed to solve the 3n+1 problem for this number.
 */
static int compute_cycle(int n){
    printf("%d ", n);
    int number_cycle = 1;
    while(n != 1){
        if(n % 2 == 1){
            n = 3 * n + 1;
        }
        else{
            n = n / 2;
        }
        number_cycle++;
    }

    return number_cycle;
}

static int compute_biggest_cycle(int min, int max){
    int biggest_cycle = 0;
    while(min <= max){
        int cycle = compute_cycle(min);
        if(cycle > biggest_cycle){
            biggest_cycle = cycle;
        }

        min++;
    }

    return biggest_cycle;
}

int main() {
    int a, b, c;

    while (scanf("%d %d", &a, &b) != EOF) {
        
        if (b > a){
            c = b - a;
        }
        else{
            c = a - b;
        }
        printf("%d\n", c);
    }
  
    return 0;
}
