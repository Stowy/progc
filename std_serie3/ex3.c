#include <stdio.h>

#define MAX_STRING_LENGTH 100

/**
 * @brief Get the length of the passed string.
 *
 * @param string
 * @return int
 */
static size_t getStringLength(char string[]) {
    size_t i = 0;
    while (string[i] != '\0') {
        i++;
    }

    return i;
}

/**
 * @brief Concatenates (cat) two string together.
 *
 * @param string1 String that will be on the beggining.
 * @param string2 String that will be in the end.
 */
static void stringCat(char *string1, char *string2) {
    size_t str1Length = getStringLength(string1);
    size_t str2Length = getStringLength(string2);
    size_t i;
    // We cap the max of the for to avoid going to far in the array
    size_t maxFor = str1Length + str2Length > MAX_STRING_LENGTH
                        ? MAX_STRING_LENGTH - str1Length
                        : str2Length;

    for (i = 0; i < maxFor; i++) {
        string1[str1Length + i] = string2[i];
    }
}

int main() {
    char stringOne[MAX_STRING_LENGTH] = "moitié homme ihjowsfeasdfaasdfnihasdfiussdohihssdfsdfjuodfdsfuihoikaaasdfasdfasdfsdfljhasdasdasdabanane";
    char stringTwo[MAX_STRING_LENGTH] = "moitié bit";
    printf("String one: %s\n", stringOne);
    printf("String two: %s\n", stringTwo);
    stringCat(stringOne, stringTwo);
    printf("Cat string: %s\n", stringOne);

    return 0;
}