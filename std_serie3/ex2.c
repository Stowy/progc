#include <stdio.h>

#define ARRAY_LEN(x) (sizeof(x) / sizeof((x)[0]))
#define SENTENCE_MAX_SIZE 100

/**
 * @brief Get the length of the passed string.
 *
 * @param string
 * @return int
 */
static size_t getStringLength(char string[]) {
    size_t i = 0;
    while (string[i] != '\0') {
        i++;
    }

    return i;
}

/**
 * @brief Creates a copy of the string2 in the string1.
 * 
 * @param string1 Where string2 will be copied.
 * @param string2 String that will be copied.
 */
static void copyString(char *string1, char *string2) {
    size_t i;
    for (i = 0; i < getStringLength(string2); i++) {
        string1[i] = string2[i];
    }
}

int main() {
    char sentence[SENTENCE_MAX_SIZE];
    char copy[SENTENCE_MAX_SIZE] = "";

    printf("Type a sentence.\n");
    if (fgets(sentence, SENTENCE_MAX_SIZE, stdin) != NULL) {
        copyString(copy, sentence);
        printf("Copy of the string: %s", copy);
    }

    return 0;
}