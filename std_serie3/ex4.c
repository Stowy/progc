#include <stdio.h>

/**
 * @brief Get the length of the passed string.
 *
 * @param string
 * @return int
 */
static size_t getStringLength(char string[]) {
    size_t i = 0;
    while (string[i] != '\0') {
        i++;
    }

    return i;
}

static int compareSizeString(char *string1, char *string2){
    size_t str1Length = getStringLength(string1);
    size_t str2Length = getStringLength(string2);

    
}

int main(){

}