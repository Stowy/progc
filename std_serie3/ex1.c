#include <stdio.h>

#define SENTENCE_MAX_SIZE 100

/**
 * @brief Get the length of the passed string.
 *
 * @param string
 * @return int
 */
static int getStringLength(char string[]) {
    int i = 0;
    while (string[i] != '\0') {
        i++;
    }

    return i;
}

/**
 * @brief Get the number of f's in the string.
 *
 * @param string
 * @return int
 */
static int getNbrF(char string[]) {
    int numberOfF = 0;
    int i;

    for (i = 0; i < getStringLength(string); i++) {
        if (string[i] == 'f' || string[i] == 'F') {
            numberOfF++;
        }
    }

    return numberOfF;
}

int main() {
    char sentence[SENTENCE_MAX_SIZE] = "";
    int numberOfF;

    printf("Type a sentence.\n");
    if (fgets(sentence, SENTENCE_MAX_SIZE, stdin) != NULL) {
        numberOfF = getNbrF(sentence);
        printf("There is %d f in your sentence.\n", numberOfF);
    }

    return 0;
}