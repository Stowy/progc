/**
 * \file temp_measurement.c
 * \brief Multi-platform temperature measurement application.
 *         
 *  You have to pass the version and the platform with -D when you compile:
 *  gcc -DPLATFORM_CODE -DVERSION=251 -o temp_measurement temp_measurement.c
 *  
 *  These are the plaform codes:
 *  WIN32 : for 32 bit windows
 *  WIN64 : for 64 bit windows
 *  ARDUINO : for arduino
 *  
 *  Compilation exemple:
 *  gcc -DWIN32 -DVERSION=200 -o temp_measurement temp_measurement.c
 *  gcc -DARDUINO -DVERSION=140 -o temp_measurement temp_measurement.c
 *
 * \author Fabian HUBER
 * \date 30.09.2019, V1
 */

#include <stdio.h>

//Checks on what plaform we are and sets the pseudo-constant BUS_SIZE accordingly
#ifdef WIN32
    #define BUS_SIZE 32
#elif defined WIN64
    #define BUS_SIZE 64
#elif defined ARDUINO
    #define BUS_SIZE 16
#else
    #error Unknown Platform    
#endif

//Checks on what version we are and sets the pseudo-constant NB_SAMPLES_MAX accordingly
#if VERSION > 0 && VERSION < 300
    #define NB_SAMPLES_MAX 100
#elif VERSION >= 300 && VERSION <= 730
    #define NB_SAMPLES_MAX 200
#elif VERSION > 730
    #define NB_SAMPLES_MAX 1000
#else
    #error Unknown version
#endif

int temperature_samples[NB_SAMPLES_MAX];

int main(void){
    printf("BUS_SIZE : %d\n", BUS_SIZE);
    printf("Version : %d\n", VERSION);
    printf("Nb samples max = %d\n", NB_SAMPLES_MAX);
    return 0;
}