#!/bin/bash
export PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig

gcc -Wall -o hello_allegro hello_allegro.c $(pkg-config --cflags --libs allegro-5 allegro_font-5 allegro_ttf-5)