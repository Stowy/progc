/* $Id: hello_allegro.c 8852 2019-03-06 20:41:44Z marechal $ */
/* CFPT-EI, C. Marechal, 12/2016
minimal source for testing allegro installation */
/*
package : apt-get install liballegro5-dev
compilation :
export PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig
gcc -Wall -o hello_allegro hello_allegro.c $(pkg-config
--cflags --libs allegro-5 allegro_font-5 allegro_ttf-5)
 */

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <stdio.h>

#define FONT_FILENAME "pirulen.ttf"

int main(int argc, char *argv[])
{
    /* ALLEGRO_DISPLAY is an opaque type representing an open
    splay or window */
    ALLEGRO_DISPLAY *display = NULL;
    /* A handle identifying any kind of font */
    ALLEGRO_FONT *font = NULL;
    /* structure which describes a color in a device independant
    way */
    ALLEGRO_COLOR backgroundColor, textColor;

    /* initialize the Allegro system. No other Allegro functions
    n be called before al_init() */
    /* returns true if Allegro was successfully initialized */
    if (al_init() == false)
    {
        fprintf(stderr, "failed to initialize allegro!\n");
        return EXIT_FAILURE;
    }

    al_init_font_addon();
    // True Type Font addon
    if (al_init_ttf_addon() == false)
    {
        fprintf(stderr, "failed to init ttf addon!\n");
        return EXIT_FAILURE;
    }

    /* create a display, or window, with the specified dimensions */
    /* returns NULL on error */
    display = al_create_display(640, 480);
    if (display == NULL)
    {
        fprintf(stderr, "failed to create display!\n");
        return EXIT_FAILURE;
    }
    /* loads a TrueType font from a file using the FreeType library */
    /* file *.ttf must be in the the same directory as the ←-
    executable */
    font = al_load_ttf_font(FONT_FILENAME, 50, 0);
    if (font == NULL)
    {
        fprintf(stderr, "Could not load ’%s’.\n", FONT_FILENAME);
        return EXIT_FAILURE;
    }
    backgroundColor = al_map_rgb(50, 10, 70);
    /* clear the complete target bitmap with background color (RGB) */
    al_clear_to_color(backgroundColor);
    textColor = al_map_rgb(255, 255, 255);
    /* Writes the 0-terminated string text onto bitmap at position ←-
    x, y, using the specified font */
    al_draw_text(font,
                 textColor,
                 640 / 2,
                 480 / 4,
                 ALLEGRO_ALIGN_CENTRE,
                 "Hello Allegro");
    /* copies or updates the front and back buffers so that what ←-
    has been drawn
     previously on the currently selected display becomes visible ←-
    on screen */
    al_flip_display();

    /* waits for the specified number seconds */
    al_rest(10.0);

    al_destroy_display(display);

    return EXIT_SUCCESS;
}