/* # $Id: gcd1.c 3993 2012-10-15 21:05:09Z marechal $ */
#include <sys/time.h>
#include "tests.h" /* macro-commands for simpleCtest */

/*@unused@*/
static unsigned int gcd(unsigned int a, unsigned int b) {
    while (b != 0){
        unsigned int r = a % b;
        a = b;
        b = r;
    }

    return a;
}

#ifdef UNIT_TESTS
/* Start the overall test suite */
START_TESTS()

struct timeval start, stop;
double duration;

(void)gettimeofday(&start,NULL);

int i;
for(i=0; i < 1000; ++i) {
/* A new group of tests, with an identifier */
START_TEST("0 cases")
	ASSERT(gcd(0,1) == 1);
	ASSERT(gcd(0,17) == 17);
	ASSERT(gcd(0,0) == 0);
END_TEST()
START_TEST("same numbers")
	ASSERT(gcd(1,1) == 1);
	ASSERT(gcd(10,10) == 10);
	ASSERT(gcd(65535,65535) == 65535);
	ASSERT(gcd((unsigned int)((1 << 30)-1),(unsigned int)((1 << 30) -1)) == (unsigned int)(1 << 30)-1);
END_TEST()

START_TEST("big numbers")
	ASSERT(gcd((unsigned int)4294967295,127) == 1);
	ASSERT(gcd(1000000007,1000000009) == 1);
END_TEST()

START_TEST("prime numbers")
	ASSERT(gcd(23,101) == 1);
END_TEST()

START_TEST("multiple numbers")
	ASSERT(gcd(456, 456*123) == 456);
END_TEST()
} /* end for */

(void)gettimeofday(&stop,NULL);
duration = (double)((stop.tv_usec - start.tv_usec) / 1000000. + stop.tv_sec - start.tv_sec);
printf("Duration [s] = %f\n", duration);

/*/ End the overall test suite */
END_TESTS()
#endif