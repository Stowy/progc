#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE 10

int getSumArray(int array[ARRAY_SIZE]) {
    int i;
    int sum = 0;
    for (i = 0; i < ARRAY_SIZE; i++)
    {
        sum += array[i];
    }
    
    return sum;
}

int main() {
    int array[ARRAY_SIZE] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int sum = getSumArray(array);
    printf("Sum: %d\n", sum);
    return 0;
}