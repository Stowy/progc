/**
 * @file ex2.c
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief program that display the max value in an array
 * @version 0.1
 * @date 2019-12-12
 * 
 * @copyright CFPT (c) 2019
 *
 */
#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE 20

int getMaxValue(int array[ARRAY_SIZE]) {
    int i;
    int maxValue = 0;
    for (i = 0; i < ARRAY_SIZE; i++)
    {
        if(maxValue < array[i]){
            maxValue = array[i];
        }
    }
    
    return maxValue;
}

int main() {
    int array[ARRAY_SIZE] = {2, 3,   4,  5,    6,    7,   8,    2,    346,  6,   57,
                   3, 234, 57, 2345, 9345, 325, 2355, 4623, 2347, 9347};
    int maxValue = getMaxValue(array);
    printf("maxValue: %d\n", maxValue);
}