/**
 * @file ex1.c
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief program that display elements of odds indexes in an array
 * @version 0.1
 * @date 2019-12-12
 * 
 * @copyright CFPT (c) 2019
 * 
 */
#include <stdio.h>
#include <stdlib.h>

#define TAB_LENGTH 20

static void displayOddElements(int tab[TAB_LENGTH]) {
    int i;
    for (i = 0; i < TAB_LENGTH; i++) {
        if (i % 2 != 0) {
            printf("Element [%d]: %d\n", i, tab[i]);
        }
    }
}

int main() {
    int tab[20] = {12,  45, 34, 12,  23,   23,   6,    2346, 21,  234,
                   534, 23, 5,  234, 2345, 2345, 1235, 754,  346, 12354};
    displayOddElements(tab);
    return 0;
}