/**
 * @file huber_WERTYU_V2.c
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief WERTYU challenge of onlinejudge.org
 *
 * Usage : ./exefile < testfile.txt
 * testfile.txt contains a string which charactesr are shifted
 * of one row in a qwerty keyboard
 *
 * @version 0.2
 * @date 2019-12-16
 *
 * @copyright CFPT (c) 2019
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INPUT_KEYBOARD_ARRAY_MAX 256

int main() {
    /* getline() execute a malloc if destination buffer pointer is NULL */
    char *sentence = NULL;
    char dictionnary[INPUT_KEYBOARD_ARRAY_MAX] = {
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ';', ' ', ' ',
        ' ', ' ', 'M', '0', ',', '.', '9', '`', '1', '2', '3', '4', '5', '6',
        '7', '8', ' ', 'L', ' ', '-', ' ', ' ', ' ', ' ', 'V', 'X', 'S', 'W',
        'D', 'F', 'G', 'U', 'H', 'J', 'K', 'N', 'B', 'I', 'O', ' ', 'E', 'A',
        'R', 'Y', 'C', 'Q', 'Z', 'T', ' ', 'P', ']', '[', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
        ' ', ' ', ' ', ' ',
    };
    unsigned int i;
    size_t nbbytes;

    /* unrecog is a splint directive in order to avoid warning on getline
     * function
     */
    while (/*@-unrecog@*/ getline(&sentence, &nbbytes, stdin) != EOF) {
        /* sentence contains all characters of one line */
        for (i = 0; i < /*@-null@*/ (unsigned int)strlen(sentence); ++i) {
            printf("%c", dictionnary[(int)sentence[i]]);
        }

    } /* end while */

    /* dealllocate memory allocated automatically by getline() */
    free(sentence);
    return 0;
}
