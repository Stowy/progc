#ifndef FUNC_H
#define FUNC_H

#include "main.h"
#include <stdio.h>

ArrayType *CreateArray(size_t n);
void DestroyArray(ArrayType *t);
void ReadArray(ArrayType *t);
void Display(ArrayType *t);
ArrayType *DoubleArray(ArrayType *t);

#endif // !FUNC_H
