#include "main.h"
#include "func.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{
    ArrayType *array;
    ArrayType *doubleArray;
    
    array = CreateArray(ARRAY_SIZE);
    ReadArray(array);

    printf("Content of the simple array: \n");

    Display(array);
    doubleArray = DoubleArray(array);

    printf("Content of the double array: \n");

    Display(doubleArray);

    free(array->tab);
    free(array);
    free(doubleArray->tab);
    free(doubleArray);

    return 0;
}