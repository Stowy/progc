#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>

#define ARRAY_SIZE 10

typedef struct ArrayType_s
{
    size_t nb_elem;
    int *tab;
} ArrayType;

#endif // !MAIN_H