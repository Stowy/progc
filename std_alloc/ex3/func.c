#include "func.h"
#include "main.h"
#include <stdio.h>
#include <stdlib.h>

ArrayType *CreateArray(size_t n)
{
    size_t i;
    ArrayType *array;

    //Allocate memory and add valute to nb_elem
    array = (ArrayType *)malloc(sizeof(ArrayType));
    if (array == NULL)
    {
        exit(EXIT_FAILURE);
    }
    array->nb_elem = n;
    array->tab = (int *)calloc(n, sizeof(int));
    if (array->tab == NULL)
    {
        exit(EXIT_FAILURE);
    }

    //Init array
    for (i = 0; i < n; i++)
    {
        array->tab[i] = 0;
    }

    return array;
}

void DestroyArray(ArrayType *t)
{
    if (t == NULL)
    {
        return;
    }
    free(t->tab);
    free(t);
}

void ReadArray(ArrayType *t)
{
    int inputNumber;
    size_t i;

    printf("Veuillez entrez les valeurs pour le tableau:\n");

    for (i = 0; i < t->nb_elem; i++)
    {
        printf("Veuillez entrer la valeur %zu: ", i);
        scanf("%d", &inputNumber);
        t->tab[i] = inputNumber;
    }
}

void Display(ArrayType *t)
{
    size_t i;

    printf("Contenu du tableau: \n");

    for (i = 0; i < t->nb_elem; i++)
    {
        printf("Élément à l'index %zu: %d\n", i, t->tab[i]);
    }
}

ArrayType *DoubleArray(ArrayType *t)
{
    ArrayType *doubleArray;
    size_t i;

    doubleArray = CreateArray(t->nb_elem);

    for (i = 0; i < doubleArray->nb_elem; i++)
    {
        doubleArray->tab[i] = t->tab[i] * 2;
    }

    return doubleArray;
}