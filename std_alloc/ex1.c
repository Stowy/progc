#include <stdio.h>
#include <stdlib.h>

int *get_tab()
{
    int *tab;
    int size;

    scanf("%d", &size);
    tab = calloc(size, sizeof(int));

    return tab;
}

int main()
{
    int *tab = get_tab();

    free(tab);
}