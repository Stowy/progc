#include "../Headers/traitement.h"
#include <stdio.h>
#include <string.h>

#define MAX_SENTENCE_LENGTH 100
#define MAX_WORD_LENGTH 50

void letterOccurence(char sentence[])
{
    char selectedLetter;
    printf("Tappez une lettre a chercher: ");
    scanf("%c", &selectedLetter);
    emptyScanfBuffer();
    printf("Il y a %i lettre \"%c\" dans dans cette phrase.\n",
           getOccurence(sentence, selectedLetter), selectedLetter);
}

void letterRemoval(char sentence[])
{
    char newSentence[MAX_SENTENCE_LENGTH];
    char selectedLetter;
    printf("Tappez une lettre a enlever: ");
    scanf("%c", &selectedLetter);
    emptyScanfBuffer();
    deleteLetter(sentence, selectedLetter, newSentence);
    printf("%s\n", newSentence);
}

void wordFinding(char sentence[])
{
    char selectedWordWithNewline[MAX_WORD_LENGTH];
    char selectedWordWithoutNewline[MAX_WORD_LENGTH];

    printf("Tapez un mot a trouver: ");
    fgets(selectedWordWithNewline, MAX_WORD_LENGTH, stdin);
    deleteLetter(selectedWordWithNewline, '\n', selectedWordWithoutNewline);
    if (isWordInSentence(sentence, selectedWordWithoutNewline))
    {
        printf("Le mot \"%s\" se trouve dans la phrase.\n", selectedWordWithoutNewline);
    }
    else
    {
        printf("Le mot \"%s\" ne se trouve pas dans la phrase.\n", selectedWordWithoutNewline);
    }
}

int main()
{
    char sentence[MAX_SENTENCE_LENGTH];
    int selection;

    printf("Tapez une phrase.\n");
    fgets(sentence, MAX_SENTENCE_LENGTH, stdin);
    printf("1.Trouve le nombre d’occurrence d’une lettre\n2.Efface une "
           "lettre\n3.Contient le mot?\n4.Pangramme\n5.Quitte\n");
    scanf("%i", &selection);
    emptyScanfBuffer();

    if (selection == 1)
    {
        letterOccurence(sentence);
    }
    else if (selection == 2)
    {
        letterRemoval(sentence);
    }
    else if (selection == 3)
    {
        wordFinding(sentence);
    }
    else if (selection == 4)
    {
    }
    else if (selection == 5)
    {
        printf("Fermeture du programme...\n");
    }

    return 0;
}