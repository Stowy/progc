#include "../Headers/traitement.h"
#include <ctype.h>
#include <stdio.h>
#include <string.h>

int getOccurence(char sentence[], char letter)
{
    int quantity = 0;
    size_t i;
    for (i = 0; i < strlen(sentence); i++)
    {
        if (sentence[i] == letter)
        {
            quantity++;
        }
    }

    return quantity;
}

void emptyScanfBuffer()
{
    scanf("%*[^\n]");
    getchar();
}

void deleteLetter(char sentence[], char letter, char *newSentence)
{
    // size of the new sentence: size of the old sentence - amount of the letter
    // we want to remove in the old sentence
    size_t i;
    size_t nbRemovedLetter = 0;

    for (i = 0; i < strlen(sentence); i++)
    {
        if (sentence[i] != letter)
        {
            // i - nbrRemovedLetter because we don't want empty spaces in our
            // array
            newSentence[i - nbRemovedLetter] = sentence[i];
        }
        else
        {
            nbRemovedLetter++;
        }
    }
}

int isWordInSentence(char sentence[], char word[])
{
    int nbrSameLetter = 0;
    size_t i, j;
    for (i = 0; i < strlen(sentence); i++)
    {
        for (j = 0; j < strlen(word); j++)
        {
            if (sentence[i + j] == word[j])
            {
                // we increment every time there is the same letter in the
                // sentence and in the word
                nbrSameLetter++;
            }
            else
            {
                // if a letter is different we put it back to 0
                nbrSameLetter = 0;
            }
        }
        // if the amount of same letter equals to the length of the word, that
        // means it is the the sentence
        if (nbrSameLetter == strlen(word))
        {
            return 1;
        }
    }

    return 0;
}

int isPangramme(char sentence[])
{
    char alphabet[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                       'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
                       's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    size_t i;

    for (i = 0; i < strlen(sentence); i++)
    {
        sentence[i] = tolower(sentence[i]);
    }
}