#ifndef TRAITEMENT_H
#define TRAITEMENT_H

int getOccurence(char sentence[], char letter);
void emptyScanfBuffer();
void deleteLetter(char sentence[], char letter, char *newSentence);
int isWordInSentence(char sentence[], char word[]);

#endif /* TRAITEMENT_H */
       /* EOF */