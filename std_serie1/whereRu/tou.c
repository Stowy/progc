#include <stdio.h>

int main(){
    float number;
    printf("Type a decimal number\n");
    scanf("%f", &number);
    printf("Number: %f\n", number);
    printf("%d\n", (int)number);
    printf("%p\n", &number);

    return 0;
}