#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static int computePerimeter(int radius){
   return (int)(2 * M_PI * radius);
}

static int computeArea(int radius){
    return (int)pow(M_PI * radius, 2);
}

int main(){
    int radius;
    printf("Type a radius\n");
    (void)scanf("%d", &radius);
    printf("Perimeter: %d\n", computePerimeter(radius));
    printf("Area: %d\n", computeArea(radius));

    return 0;
}