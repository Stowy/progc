#include <stdio.h>

int main(){
    int inputNumber;
    printf("Type a number\n");
    scanf("%d", &inputNumber);
    //Prints 1 if the number is odd and 0 if it's even
    printf("%d\n", inputNumber % 2 != 0);

    return 0;
}