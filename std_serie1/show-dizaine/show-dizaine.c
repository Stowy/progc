#include <stdio.h>
#include <math.h>

int main(){
    int number = 0;
    //the number by wich we will do the modulo on the number
    int reductor = 1;
    int modulatedNumber;
    size_t numberOfNumber;
    size_t i;

    printf("Type a number\n");
    scanf("%d", &number);

    //Gets the number of numbers in number
    numberOfNumber = (size_t)(1+log10(number));
    printf("decomposition: \n");
    for (i = 0; i < numberOfNumber; i++)
    {
        modulatedNumber = number / reductor; 
        modulatedNumber %=  10;
        printf("%d\n", modulatedNumber);
        reductor *= 10;
    }
}