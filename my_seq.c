/**
 * @file enormeESeq.c
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Print a sequence of numbers, just like the command seq
 * @version 1.0
 * @date 2019-10-28
 * 
 * @copyright Copyright (c) 2019
 * 
 */
#include <stdio.h>
#include <stdlib.h>

/* Constants */
#define MODE_ONE 2
#define MODE_TWO 3
#define MODE_THREE 4

#define DEFAULT_INCREMENT 1
#define DEFAULT_FIRST 1
#define DEFAULT_LAST 0

/**
 * @brief Compute the sequence
 * 
 * @param first The number where the sequence will start.
 * @param increment By how much the number will be incremented. 
 * @param last The number where the sequence will end.
 */
static void compute_seq(int first, int increment, int last)
{
    /* the length of the sequence */
    int nbr_seq = (last - first) / increment;
    
    /* number that will be added in the array */
    int nbr = first;
    int i;

    for (i = 0; i <= nbr_seq; i++)
    {
        printf("%d\n", nbr);
        nbr += increment;
    }
}

/**
 * @brief Main function
 * 
 * @param argc Number of arguments.
 * @param argv Array of arguments.
 * @return int 
 */
int main(int argc, char *argv[])
{
    int increment = DEFAULT_INCREMENT;
    int first = DEFAULT_FIRST;
    int last = DEFAULT_LAST;

    /* Read the arguments */
    switch (argc)
    {
    case MODE_ONE:
        last = atoi(argv[1]);
        break;
    case MODE_TWO:
        first = atoi(argv[1]);
        last = atoi(argv[2]);
        break;
    case MODE_THREE:
        first = atoi(argv[1]);
        increment = atoi(argv[2]);
        last = atoi(argv[3]);
        break;
    default:
        /* Print out an error and exits the program */
        fprintf(
            stderr,
            "Bad arguments.\n"
            "Usage: my_seq [OPTION]... LAST\n"
            "  or:  my_seq [OPTION]... FIRST LAST\n"
            "  or:  my_seq [OPTION]... FIRST INCREMENT LAST\n"
            "Print numbers from FIRST to LAST, in steps of INCREMENT.\n");
        exit(EXIT_FAILURE);
    }

    compute_seq(first, increment, last);

    return 0;
}