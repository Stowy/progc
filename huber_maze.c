/* 
 \file huber_maze.c
 \brief Maze generator with size constraints.
        You have to pass the size you want with -D when you compile with a pseudo-constant named MAZE_REQ_SIZE.
        Ex: gcc -DMAZE_REQ_SIZE=16 -o maze maze.c
 \author Fabian HUBER
 \date 23.09.2019, V2
*/

#include <stdio.h>

#define MAZE_MAX_SIZE 100
#define MAZE_DEF_SIZE 20

//Check if the pseudo-constant is set and superior than 0
#if (MAZE_REQ_SIZE) <= 0 || !(MAZE_REQ_SIZE)
    #define MAZE_REAL_SIZE MAZE_DEF_SIZE
//Check if the requested size is superior than the max
#elif MAZE_REQ_SIZE > MAZE_MAX_SIZE
    #define MAZE_REAL_SIZE MAZE_MAX_SIZE
#else
    //Make sure the labyrith is even
    #define MAZE_REAL_SIZE (MAZE_REQ_SIZE)+((MAZE_REQ_SIZE) % 2)
#endif

int maze[MAZE_REAL_SIZE][MAZE_REAL_SIZE];

int main(void){
    printf("Maze size = %d\n", MAZE_REAL_SIZE);
    return 0;
}